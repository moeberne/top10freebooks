# Top10FreeBooks #

Top10FreeBooks simple iOS app which can parse (and display book cover images from) an iTunes RSS feed of the top n books in any category. 

Currently, the app displays book cover images from the top 10 free books in Australia in the Arts & Entertainment category. 

A different category and number of books can be loaded by replacing the XML_URL constant in TFBXMLParser.m with a different RSS url. Various RSS urls can be generated from https://rss.itunes.apple.com/

### The purpose of this app ###

* To demonstrate the use of NSURLSession w/ completion blocks for asynchronous image downloading and caching in a table view
* To demonstrate the use of a custom delegate within NSXMLParser
* To demonstrate a simple usage of auto layout (fixed height, dynamic width images)

### How do I get set up? ###

Just clone the repo and open the project in XCode. Happy reading!