//
//  TFBXMLParser.m
//  Top10FreeBooks
//
//  Created by Moe Burney on 14/06/2015.
//  Copyright (c) 2015 Moe Burney. All rights reserved.
//

// This class provides a parser object which can be used to parse book cover images from an iTunes xml feed.
// A delegate object can listen to the delegate messages for completion and errors

#import "TFBXMLParser.h"

static NSString* const XML_URL = @"https://itunes.apple.com/au/rss/topfreeebooks/limit=10/genre=9007/xml";
static NSString* const XML_TITLE_ELEMENT = @"title";
static NSString* const XML_IMAGE_ELEMENT = @"im:image";
static NSString* const XML_IMAGE_ELEMENT_ATTRIBUTE = @"height";
static NSString* const XML_IMAGE_HEIGHT = @"170";

@interface TFBXMLParser ()
{
    NSMutableString *link;
    NSMutableString *title;
    NSString *element;
    NSString *attribute;
}
@end

@implementation TFBXMLParser

 - (id)init
{
    return [self initWithUrl:XML_URL];
 }

// Designated initializer
- (id)initWithUrl:(NSString*)urlString
{
    self = [super init];
    if (self != nil)
    {
        self.feedTitle = nil;
        NSURL *url = [NSURL URLWithString:urlString];
        
        self.imageUrls = [[NSMutableArray alloc] init];
        
        // Parse the given XML feed so we can return results to the delegate
        self.parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
        [self.parser setDelegate:self];
        [self.parser setShouldResolveExternalEntities:NO];
    }
    return self;
}

- (void)parse
{
    [self.parser parse];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    element = elementName;
    
    attribute = [attributeDict objectForKey:XML_IMAGE_ELEMENT_ATTRIBUTE];
    
    if ([attribute isEqualToString:XML_IMAGE_HEIGHT])
    {
        link = [[NSMutableString alloc] init];
    }
    
    if ([element isEqualToString:XML_TITLE_ELEMENT] && !self.feedTitle)
    {
        
        title = [[NSMutableString alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSString *trimmedString = [string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    NSString *trimmedString1 = [trimmedString stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    
    if (([element isEqualToString:XML_IMAGE_ELEMENT]) && ([attribute isEqualToString:XML_IMAGE_HEIGHT]))
    {
        [link appendString:trimmedString1];
    }
    
    if ([element isEqualToString:XML_TITLE_ELEMENT] && !self.feedTitle)
    {
        [title appendString:trimmedString1];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if (([element isEqualToString:XML_IMAGE_ELEMENT]) && ([attribute isEqualToString:XML_IMAGE_HEIGHT]))
    {
        [self.imageUrls addObject:link];
    }
    
    if ([element isEqualToString:XML_TITLE_ELEMENT] && !self.feedTitle)
    {
        self.feedTitle = title;
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    [self.delegate parserDidFinish:self];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self.delegate parser:self didFailWithError:error];
}


@end
