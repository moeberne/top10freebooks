//
//  TFBTableViewController.h
//  Top10FreeBooks
//
//  Created by Moe Burney on 14/06/2015.
//  Copyright (c) 2015 Moe Burney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFBXMLParser.h"
#import "TFBDownloader.h"
@interface TFBTableViewController : UITableViewController<TFBXMLParserDelegate>

@property (strong, nonatomic) NSArray *model;
@property (strong, nonatomic) TFBXMLParser *parser;
@property (strong, nonatomic) TFBDownloader *downloader;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@end
