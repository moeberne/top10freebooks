//
//  TFBDownloader.h
//  Top10FreeBooks
//
//  Created by Moe Burney on 14/06/2015.
//  Copyright (c) 2015 Moe Burney. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TFBDownloader : NSObject

- (id) initWithConfiguration: (NSURLSessionConfiguration*) config;
- (NSURLSessionTask*) download:(NSString*)s
             completionHandler:(void(^)(NSURL* url))ch;
- (void) cancelAllTasks;

@end
