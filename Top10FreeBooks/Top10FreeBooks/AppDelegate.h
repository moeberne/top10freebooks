//
//  AppDelegate.h
//  Top10FreeBooks
//
//  Created by Moe Burney on 14/06/2015.
//  Copyright (c) 2015 Moe Burney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

