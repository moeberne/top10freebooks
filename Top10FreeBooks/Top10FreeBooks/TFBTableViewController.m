//
//  TFBTableViewController.m
//  Top10FreeBooks
//
//  Created by Moe Burney on 14/06/2015.
//  Copyright (c) 2015 Moe Burney. All rights reserved.
//

// This view controller class parses an iTunes XML feed of books to retrieve image urls
// of book covers contained in the feed.

#import "TFBTableViewController.h"
#import "TFBDownloader.h"
#import "TFBCell.h"
#import "TFBXMLParser.h"

const double DEFAULT_CELL_HEIGHT = 200.0;
static NSString* const DEFAULT_CELL_IDENTIFIER = @"TFBCell";
static NSString* const MODEL_IMAGE_URL = @"url";
static NSString* const MODEL_IMAGE = @"im";
static NSString* const MODEL_IMAGE_DOWNLOAD_TASK = @"task";
static NSString* const MODEL_IMAGE_WIDTH = @"width";

@interface TFBTableViewController ()

@end

@implementation TFBTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.model = [[NSArray alloc] init];
    
    UINib *nib = [UINib nibWithNibName:@"TFBCell" bundle:nil];
    [[self tableView] registerNib:nib forCellReuseIdentifier:DEFAULT_CELL_IDENTIFIER];
    
    [self.tableView setContentInset:UIEdgeInsetsMake(30,0,0,0)];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = DEFAULT_CELL_HEIGHT;
    [self loadDataIntoTable];
}

// Parse the XML feed, get image urls, then load images into table
- (void)loadDataIntoTable
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    self.parser = [[TFBXMLParser alloc] init];
    self.parser.delegate = self;
    [self.parser parse];
    

    // If the parser didn't get any data, ask to retry
    if ([self.model count] <= 0)
    {
        [self noData];
    }
    else
    {
        [self.tableView reloadData];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return self.parser.feedTitle;
}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.textColor = [UIColor darkGrayColor];
    header.textLabel.font = [UIFont boldSystemFontOfSize:12];
    CGRect headerFrame = header.frame;
    header.textLabel.frame = headerFrame;
    header.textLabel.text = self.parser.feedTitle;
    header.textLabel.textAlignment = NSTextAlignmentCenter;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.model.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TFBCell *cell = [tableView dequeueReusableCellWithIdentifier:DEFAULT_CELL_IDENTIFIER];
   
    NSMutableDictionary *d = (self.model)[indexPath.row];
    
    // If we have an image saved in our model, display it
    if (d[MODEL_IMAGE])
    {
        cell.spinner.hidden = YES;
        double newImageWidth = [d[MODEL_IMAGE_WIDTH] doubleValue];
        cell.bookImage.image = d[MODEL_IMAGE];
        cell.cellWidth = [NSLayoutConstraint constraintWithItem:cell.bookImage
                                                            attribute:NSLayoutAttributeWidth
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:nil
                                                            attribute:NSLayoutAttributeNotAnAttribute
                                                           multiplier:1.0
                                                             constant:newImageWidth];
    }
    // If we don't have an image yet, download it, save it to the model, and then reload the cell
    else
    {
        cell.bookImage.image = nil;

        [cell.spinner startAnimating];
        cell.spinner.hidden = NO;
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        TFBDownloader *downloader = [[TFBDownloader alloc] initWithConfiguration:config];
        self.downloader = downloader;
        NSString *urlString = d[MODEL_IMAGE_URL];
        
        // Download image asynchronously with our downloader class
        [downloader download:urlString completionHandler:^(NSURL* url){
            if (!url)
                return;
            NSData* data = [NSData dataWithContentsOfURL:url];
            UIImage* im = [UIImage imageWithData:data];
            d[MODEL_IMAGE] = im;
            d[MODEL_IMAGE_WIDTH] = [NSNumber numberWithDouble:im.size.width];
            
            // Once we have downloaded the image, reload cell in the next main run loop
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadRowsAtIndexPaths:@[indexPath]
                                      withRowAnimation:UITableViewRowAnimationNone];
            });
        }];
    }
    return cell;
}

// Cancel the image download if the user scrolls away from the cell in middle of download
-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *d = self.model[indexPath.row];
    NSURLSessionTask* task = d[MODEL_IMAGE_DOWNLOAD_TASK];
    if (task && task.state == NSURLSessionTaskStateRunning)
    {
        [task cancel];
        [d removeObjectForKey:MODEL_IMAGE_DOWNLOAD_TASK];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return DEFAULT_CELL_HEIGHT;
}

// Got the XML data, now load image urls into model and display the images
- (void)parserDidFinish:(TFBXMLParser*)parser
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    for (NSString* url in self.parser.imageUrls)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:url forKey:MODEL_IMAGE_URL];
        [arr addObject:dict];
    }
    
    self.model = [arr copy];
}

// Failed to get XML data, inform user
- (void)parser:(TFBXMLParser*)parser didFailWithError:(NSError*)error
{
    [self noData];
}

// No data to load for the user, ask to retry
-(void)noData
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Sorry..."
                                                                   message:@"We couldn't fetch the list of books."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *tryAgainAction = [UIAlertAction actionWithTitle:@"Try Again" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {[self loadDataIntoTable];}];
    
    [alert addAction:tryAgainAction];
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:alert animated:YES completion:nil];
    });
}

// Cancel all downloads and clean up the downloader objects to avoid memory problems
- (void)dealloc
{
    if (self->_downloader)
        [self->_downloader cancelAllTasks];
}



@end
