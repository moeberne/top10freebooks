//
//  TFBXMLParser.h
//  Top10FreeBooks
//
//  Created by Moe Burney on 14/06/2015.
//  Copyright (c) 2015 Moe Burney. All rights reserved.
//

#import <Foundation/Foundation.h>
@class TFBXMLParser;

@protocol TFBXMLParserDelegate <NSObject>
- (void)parserDidFinish:(TFBXMLParser*)parser;
- (void)parser:(TFBXMLParser*)parser didFailWithError:(NSError*)error;
@end

@interface TFBXMLParser : NSObject<NSXMLParserDelegate>

@property (nonatomic, assign) id<TFBXMLParserDelegate> delegate;
@property (strong, nonatomic) NSMutableArray *imageUrls;
@property (strong, nonatomic) NSXMLParser *parser;
@property (strong, nonatomic) NSString *feedTitle;

- (id)initWithUrl:(NSString*)urlString;
- (void)parse;

@end
