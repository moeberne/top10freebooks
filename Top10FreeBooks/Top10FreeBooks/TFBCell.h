//
//  TFBCell.h
//  Top10FreeBooks
//
//  Created by Moe Burney on 14/06/2015.
//  Copyright (c) 2015 Moe Burney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TFBCell : UITableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellWidth;
@property (weak, nonatomic) IBOutlet UIImageView *bookImage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end
